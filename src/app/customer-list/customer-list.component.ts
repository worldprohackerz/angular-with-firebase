import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {CustomerComponent} from '../customer/customer.component';
import {FirebaseService} from '../shared/firebase.service'

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  tableData = [];
  headers = [
    {
      name: 'S.No',
      value:'s.no'
    },
    {
      name: 'UserName',
      value: 'un'
    },
    {
      name: 'FirstName',
      value: 'fn'
    },
    {
      name: 'LastName',
      value: 'lastName'
    },
    {
      name: 'Email',
      value: 'mail'
    },
    {
      name: 'Address',
      value: 'fn'
    },
    {
      name: 'City',
      value: 'fn'
    },
    {
      name: 'Postalcode',
      value: 'fn'
    },
    {
      name: 'country',
      value: 'fn'
    }
  ]
  constructor(private dialog: MatDialog,public firebaseservice: FirebaseService) { }

  ngOnInit(): void {
    if(this.tableData.length == 0) {
      this.getAllCustomers()
    }
  }
  getAllCustomers() {
    this.firebaseservice.getAllCustomrs().subscribe(result => {
      console.log('All customers are ',result);
     this.tableData = result
    })
  }
  CreateCustomer() {
    const dialogRef = this.dialog.open(CustomerComponent,({
      height: '445px',
      width: '750px',
    }))
    dialogRef.afterClosed().subscribe(result => {
      console.log('After closed',result)
      if(result && result.formData && result.formData.firstName) {
        this.tableData.push(result.formData);
        this.firebaseservice.CreateOperation(result.formData)
      }
    })
    }
  deleteItem(data) {
    if(data && data.id) {
      this.firebaseservice.deleteOperation(data)
    }
  }

  OnEdit(index,data) {
    if(data && data.id) {
    const dialogRef = this.dialog.open(CustomerComponent,({
        height: '445px',
        width: '750px',
      }))
      dialogRef.componentInstance.formData = data
      dialogRef.componentInstance.isEdit = true
      dialogRef.afterClosed().subscribe(res => {
        if(res && res.isEdit) {
          this.firebaseservice.updateFunctionality(res.formData)
        }
      })
    }
  }

}
