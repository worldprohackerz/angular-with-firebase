import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {FirebaseService} from '../shared/firebase.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  studentForm = new FormGroup({
    address: new FormControl(''),
    userName: new FormControl(''),
    country: new FormControl(''),
    city:new FormControl(''),
    firstName: new FormControl(''),
    email: new FormControl(''),
    postalcode: new FormControl(''),
    lastName: new FormControl('')
  })
  constructor(public dialogRef: MatDialogRef<CustomerComponent>,
              public firebaseService : FirebaseService) { }
  isEdit: boolean = false;
  formData;
  ngOnInit(): void {
    if(this.isEdit) {
      this.studentForm.patchValue(this.formData);
    }
  }
  updateSubmit() {
    let data = this.studentForm.value
    data.id = this.formData.id
    if(this.isEdit) {
      this.firebaseService.updateFunctionality(this.studentForm.value)
      this.dialogRef.close({isEdit:this.isEdit,formData : data})
    }
  }
  close() {
    this.dialogRef.close()
  }
  CancelAll() {}
  onSubmit() {
    if(this.studentForm && this.studentForm.value){
      console.log('Form values are', this.studentForm.value)
    }
    this.dialogRef.close({isEdit:this.isEdit,formData : this.studentForm.value})

  }

}
