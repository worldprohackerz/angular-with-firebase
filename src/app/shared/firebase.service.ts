import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {removeSummaryDuplicates} from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(private db: AngularFireDatabase) {}
  getAllCustomrs() {
   return this.db.list('/Customers').valueChanges();
  }
  CreateOperation(data) {
    const dbRef = this.db.list('/Customers');
    dbRef.push(data).then(res => {
      data.id = res.key
      const database = this.db.object('/Customers/'+`${res.key}`)
      database.update(data).then(result => {

      }).catch(err => {
        console.log(err)
      })
    }).catch(err => {
      console.error(err)
    })
  }
  updateFunctionality(data) {
    if(data && data.id) {
     const dbRef = this.db.object('/Customers/'+`${data.id}`)
      dbRef.update(data).then( res=> {
        console.dir('Dir is ' ,res)
      })
    }
  }
  deleteOperation(data) {
    if(data && data.id) {
      this.db.list('/Customers/'+`${data.id}`).remove()
    }
  }
}
